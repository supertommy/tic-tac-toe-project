import express from 'express'
import cors from 'cors'
import { createServer } from 'http'
import { Server } from 'colyseus'
import { monitor } from '@colyseus/monitor'

import { TicTacToeRoom } from './TicTacToeRoom'

const app = express()

app.use(cors())
app.use(express.json())

const gameServer = new Server({
	server: createServer(app),
	express: app,
})

gameServer.define('tic-tac-toe', TicTacToeRoom)

// (optional) attach web monitoring panel
app.use('/colyseus', monitor())

gameServer.onShutdown(function(){
  console.log('game server is going down.')
});

const port = 2000
gameServer.listen(port)

console.log(`Listening on http://localhost:${port}`)
