# Teamflow Tic-Tac-Toe Project

## Overview

This is a starter project with React, [Colyseus](https://www.colyseus.io/), and TypeScript to complete a 2 player tic-tac-toe game.

The client uses React and the server is powered by Colyseus and Node.

## Objectives

1. Create a 3x3 Tic-Tac-Toe grid that responds to clicks
2. Implement server logic for Tic-Tac-Toe game
    1. game must wait for 2 players before starting
    2. only the current player can make a move
    3. changes should be reflected on both clients
3. Send data between client/server to play the game
4. Check for winner and end game when one is found
    1. detect ties or no winners
5. Store a list of winners by id, symbol (X or O), and time on the server (in memory is fine)
6. Get request to retrieve list of winners

Clone this project and then run `npm install` to begin.

### `npm run start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run start-server`

Runs the Colyseus server at [http://localhost:2000](http://localhost:2000).

The server will reload if you make edits.

